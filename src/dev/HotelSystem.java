package dev;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class HotelSystem {

    private static final HotelSystem instance = new HotelSystem();

    private final HashMap<Integer, Booking> bookings;
    private int lastBookingId = 0;

    private final HashMap<Integer, Room> rooms;
    private final HashMap<String, Customer> customers;

    private HotelSystem() {
        bookings = new HashMap<Integer, Booking>();
        rooms = new HashMap<Integer, Room>();
        customers = new HashMap<String, Customer>();
    }

    public void addCustomer(Customer customer)
    {
        customers.put(customer.getId(), customer);
    }

    public List<Customer> getCustomers() {
        return customers.values().stream().collect(Collectors.toList());
    }

    public List<Room> getRooms() {
        return rooms.values().stream().collect(Collectors.toList());
    }

    public List<Booking> getBookings() {
        return bookings.values().stream().filter(booking -> !booking.isCanceled()).collect(Collectors.toList());
    }

    public List<Booking> getBookingsOf(Room room) {
        return bookings.values().stream().filter(booking ->
                booking.getRoom().getNumber() == room.getNumber()).collect(Collectors.toList());
    }

    public void addRoom(Room room) {
        rooms.put(room.getNumber(), room);
    }

    public Booking book(Room room, Customer customer, LocalDate checkIn, LocalDate checkOut) throws Exception {
        if (checkRoomAvailability(room, checkIn, checkOut)) {
            Booking booking = new Booking(++lastBookingId, room, customer, checkIn, checkOut);

            bookings.put(lastBookingId, booking);
            return booking;
        } else {
            throw new Exception("Room N°: " + room.getNumber() + " isn't available at the given date");
        }
    }

    private boolean checkRoomAvailability(Room room, LocalDate checkIn, LocalDate checkOut) {
        List<Booking> bookingsAtRange = getBookingsFromTo(checkIn, checkOut);

        return !bookingsAtRange.stream().anyMatch(booking ->
        {
            return (booking.getRoom().getNumber() == room.getNumber() && !booking.isCanceled());
        });
    }

    public List<Customer> getCustomersAlphabetically() {
        List<Customer> customerList = new ArrayList<>(customers.values());

        Collections.sort(customerList, (a, b) -> a.getName().compareTo(b.getName()));

        return customerList;
    }

    public Booking getBooking(int bookingId) {
        return bookings.get(bookingId);
    }

    public List<Booking> getBookingsFromTo(LocalDate fromDate, LocalDate toDate) {
        List<Booking> currentBookings = getBookings();
        List<Booking> bookingsWhithinDates = currentBookings.stream().filter(booking ->
                (booking.getCheckOut().isAfter(fromDate) || booking.getCheckOut().isEqual(fromDate)) && (booking.getCheckIn().isBefore(toDate) || booking.getCheckIn().isEqual(toDate))
        ).collect(Collectors.toList());

        return bookingsWhithinDates;
    }

    public static HotelSystem instance() {
        return instance;
    }

}
