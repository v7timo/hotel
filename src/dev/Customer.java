package dev;

public class Customer
{
    private String name;
    private String id;
    private String telNumber;

    public Customer(String id, String name, String telNumber)
    {
        this.id = id;
        this.name = name;
        this.telNumber = telNumber;

        HotelSystem.instance().addCustomer(this);
    }

    public String toString()
    {
        return ("Name: "+name+" Tel: "+telNumber);
    }

    public String getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

}
