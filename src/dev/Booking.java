package dev;

import java.time.LocalDate;

public class Booking
{
    private int id;
    private LocalDate checkIn;
    private LocalDate checkOut;
    private Customer customer;
    private Room room;
    private boolean canceled;

    public Booking(int id, Room room, Customer customer, LocalDate checkIn, LocalDate checkOut) {
        this.id = id;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.customer = customer;
        this.room = room;
        this.canceled = false;
    }

    public double getPrice()
    {
        return room.calcPrice(daysCount());
    }

    public int daysCount()
    {
        return (int)checkIn.datesUntil(checkOut.plusDays(1)).count();
    }

    public boolean cancel() throws Exception
    {
        LocalDate now = LocalDate.now();

        if (checkIn.isAfter(now))
            canceled = true;
        else
            throw new Exception("Can't cancel a booking that's already in progress");

        return canceled;
    }

    public boolean isCanceled()
    {
        return canceled;
    }

    @Override
    public String toString()
    {
        return ("Room N°: "+room.getNumber()+" CheckIn: "+checkIn+" CheckOut: "+checkOut+" Price: "+getPrice());
    }

    public Room getRoom()
    {
        return room;
    }
    public LocalDate getCheckIn()
    {
        return checkIn;
    }
    public LocalDate getCheckOut()
    {
        return checkOut;
    }
}
