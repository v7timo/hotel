package dev;

import java.util.ArrayList;
import java.util.List;

public class Room
{
    private static int lastId = 0;

    protected int number;
    protected List<Amenity> amenities;
    protected double price;

    public Room(double price)
    {
        amenities = new ArrayList<Amenity>();

        number = ++lastId;
        this.price = price;

        amenities.add(Amenity.WIFI);

        HotelSystem.instance().addRoom(this);
    }

    public double calcPrice(int daysCount)
    {
        double currPrince = (price * daysCount);

        if(daysCount > 7)
            currPrince -= currPrince*0.05;

        return currPrince;
    }

    public int getNumber()
    {
        return number;
    }

    public String toString()
    {
        return ("Room Id°: "+number+" Price p/day: "+price);
    }

    public enum Amenity
    {
        SAFE_BOX,
        MINI_BAR,
        WIFI
    }
}
