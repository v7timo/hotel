package dev;

public class DeluxeRoom extends Room
{
    public DeluxeRoom(double price)
    {
        super(price);

        amenities.add(Amenity.MINI_BAR);
        amenities.add(Amenity.SAFE_BOX);
    }

    @Override
    public double calcPrice(int daysCount)
    {
        double currPrince = (price * daysCount);

        if(daysCount > 7)
            currPrince -= currPrince*0.10;

        return currPrince;
    }
}
