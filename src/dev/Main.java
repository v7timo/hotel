package dev;

import java.time.LocalDate;
import java.util.List;

public class Main
{
    public static void main(String[] args)
    {
        HotelSystem hotel = HotelSystem.instance();

        Room r1 = new Room(10);
        Room r2 = new Room(30.1);
        Room r3 = new Room(15.3);

        Customer c1 = new Customer("111111", "BJuanito", "tel1111111");
        Customer c2 = new Customer("222222", "APedrito", "tel2222222");

        try
        {
            Booking b1 = hotel.book(r1, c1, LocalDate.now(), LocalDate.of(2021, 11, 5));
            Booking b2 = hotel.book(r1, c2, LocalDate.of(2021, 12, 1), LocalDate.of(2021, 12, 20));
            Booking b3 = hotel.book(r3, c2, LocalDate.now(), LocalDate.now());

            List<Booking> asdf = hotel.getBookingsOf(r1);

            for (Booking booking : asdf) {
                System.out.println(booking);
            }

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
